import { Component, OnInit, ViewChild, ElementRef, Output, Input, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.scss']
})
export class SliderComponent implements OnInit {
  @ViewChild('sliderTrackFill') sliderTrackFill: ElementRef;
  value1: number;
  value2: number;

  @Input() min: number = 0;
  @Input() max: number = 100;
  @Input() step: number = 1;

  _minValue: number;
  @Input() set minValue(minValue: number) {
    if (minValue === null || minValue === undefined || !this.isNumber(minValue) || minValue < this.min) {
      this._minValue = this.min;
    } else {
      this._minValue = minValue;
    }
    this.minValueChange.emit(this.minValue);
  }
  get minValue(): number {
    return this._minValue;
  }
  @Output() minValueChange: EventEmitter<number> = new EventEmitter<number>();

  _maxValue: number;
  @Input() set maxValue(maxValue: number) {
    if (maxValue === null || maxValue === undefined || !this.isNumber(maxValue) || maxValue > this.max) {
      this._maxValue = this.max;
    } else {
      this._maxValue = maxValue;
    }
    this.maxValueChange.emit(this.maxValue);
  }
  get maxValue(): number {
    return this._maxValue;
  }
  @Output() maxValueChange: EventEmitter<number> = new EventEmitter<number>();

  @Output() change: EventEmitter<void> = new EventEmitter<void>();

  constructor() { }

  ngOnInit(): void {
    this.value1 = this.minValue;
    this.value2 = this.maxValue;
    this.sliderValueChange();
  }

  sliderValueChange(): void {
    const fullLength = this.max - this.min;
    const fillStart = (this.minValue - this.min) / fullLength * 100;
    const fillWidth = (this.maxValue - this.minValue) / fullLength * 100;
    this.sliderTrackFill.nativeElement.style.width = 'calc(' + fillWidth + '% - 16px)';
    this.sliderTrackFill.nativeElement.style.left = fillStart + '%';

    if (this.value1 > this.value2) {
      this.minValue = this.value2;
      this.maxValue = this.value1;
    } else {
      this.minValue = this.value1;
      this.maxValue = this.value2;
    }
  }

  isNumber(n: any): boolean {
    return !isNaN(parseFloat(n)) && isFinite(n);
  }
}
